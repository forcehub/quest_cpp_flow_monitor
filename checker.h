#ifndef FLOW_MONITOR_CHECKER_H
#define FLOW_MONITOR_CHECKER_H

#include <string>
#include <set>

/*
 * Checks the message according with predefined logic.
 * Returns bool value - if the message is suspicious (true) and we should panic, or the message is ok (false).
 * Params:
 * - encrypted_message - message string in encrypted form
 * - key - key that should be used for decryption for analysis
 * - panic_seq - set of blacklisted sequences that should cause panic
*/
bool should_panic(const std::string& encrypted_message, const std::string& key, const std::set<std::string>& panic_seq);

#endif //FLOW_MONITOR_CHECKER_H