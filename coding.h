#ifndef FLOW_MONITOR_CODING_H
#define FLOW_MONITOR_CODING_H

#include <string>

std::string encrypt(std::string& msg, std::string& key);
std::string decrypt(std::string& encrypted_msg, std::string& key);

#endif //FLOW_MONITOR_CODING_H