#include "checker.h"
#include "coding.h"

#include <chrono>

#include <iostream>

using namespace std;
using namespace std::chrono;

bool should_panic(const std::string& encrypted_message, const std::string& encryption_key, const std::set<std::string>& panic_seq) {

    // Get
    string msg = encrypted_message;
    string key = encryption_key;

    // Decrypt
    string raw_message = decrypt(msg, key);

    // Prepare
    string prepared_message = raw_message;
    for(int i = 0; i < prepared_message.length(); i++) {
        for(int j = 0; j < prepared_message.length(); j++) {
            if(prepared_message[i] < prepared_message[j]) {
                swap(prepared_message[i], prepared_message[j]);
            }
        }
    }

    // Analyze
    for(string s : panic_seq) {
        if(prepared_message.find(s) != string::npos) {
            return true;
        }
    }

    return false;
}